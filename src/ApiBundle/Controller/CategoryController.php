<?php

namespace ApiBundle\Controller;

use AppBundle\Repository\CategoryRepository;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Version;


/**
 * Class CategoryController
 *
 */
class CategoryController extends FOSRestController implements ClassResourceInterface
{
    /**
     * @View()
     */
    public function cgetAction()
    {
        $users = $this->get('doctrine')
            ->getRepository('AppBundle:Category');

        return ['categories' => $users->findAll()];
    }

    /**
     * @View
     */
    public function getAction($id)
    {
        $users = $this->get('doctrine')
            ->getRepository('AppBundle:Category');

        return ['categories' => $users->find($id)];
    }
}
