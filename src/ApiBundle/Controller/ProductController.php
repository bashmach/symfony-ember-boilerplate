<?php

namespace ApiBundle\Controller;

use AppBundle\Repository\ProductRepository;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Version;


/**
 * Class ProductController
 *
 */
class ProductController extends FOSRestController implements ClassResourceInterface
{
    /**
     * @View()
     */
    public function cgetAction()
    {
        $users = $this->getDoctrine()
            ->getRepository('AppBundle:Product');

        return ['products' => $users->findAll()];
    }

    /**
     * @View
     */
    public function getAction($id)
    {
        $users = $this->getDoctrine()
            ->getRepository('AppBundle:Product');

        return ['products' => $users->find($id)];
    }
}
