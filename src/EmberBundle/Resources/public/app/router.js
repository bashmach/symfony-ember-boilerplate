import Ember from 'ember';
import config from 'application/config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('categories', { resetNamespace: true }, function () {
    this.route('category', { path: ':id' });
  });
  this.route('products', { resetNamespace: true }, function () {
    this.route('product', { path: ':id' });
  });
});

export default Router;
