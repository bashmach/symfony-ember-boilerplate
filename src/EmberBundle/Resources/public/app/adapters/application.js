import DS from 'ember-data';
import config from 'application/config/environment';

export default DS.RESTAdapter.extend({
  namespace: config.APP.namespace,
  host: config.APP.host
});
