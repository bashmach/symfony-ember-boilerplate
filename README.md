# Symfony EmberJS Boilerplate 

[![Build Status](https://gitlab.com/bashmach/symfony-ember-boilerplate/badges/master/build.svg)](https://gitlab.com/bashmach/symfony-ember-boilerplate/builds)

This project is a fork of [ucsf-ckm/symfony-emberjs-edition](https://github.com/ucsf-ckm/symfony-emberjs-edition) with small bunch of modifications. Foremost, latest versions of Ember (~2.5) and Symfony (~3.0) plus bonuses such as 100% ready-made for CI with [GitlabCI](https://about.gitlab.com/gitlab-ci/) and deploy to [Heroku](https://www.heroku.com/about).

## What's inside

## Idea

## Install

## Run

## Deploy

### Build with GitlabCI

### Deploy to Heroku

#### Serve static

## Credits

## License

All libraries and bundles included in the boilerplate are released under the MIT or BSD license.